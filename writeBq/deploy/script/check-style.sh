#!/bin/bash

pylint -r n app/
status=${PIPESTATUS[0]}

# We need to catch error codes that are bigger then 2,
# they signal that pylint exited because of underlying error.
if [ ${status} -ge 1 ]; then
    echo "pylint exited with code ${status}, check pylint errors"
    exit ${status}
fi
exit 0