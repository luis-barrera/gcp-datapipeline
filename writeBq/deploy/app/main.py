# pylint: disable=no-member
# utf-8
import sys
import logging
import os
from google.cloud import storage
from utils.read_params import ReadParams
from utils.time_execution import TimeExecution


def upload_blob(bucket_name: str = None,
                source_file_name: str = None,
                destination_blob_name: str = None):
    LOGGER.info('Connect gcp Client.')
    storage_client = storage.Client()
    if bucket_name is not None:
        LOGGER.info('gcp assing BUCKET_NAME %s.', bucket_name)
        bucket = storage_client.bucket(bucket_name)
        blob = bucket.blob(destination_blob_name)
        LOGGER.info('gcp write jar file %s.', source_file_name)
        blob.upload_from_filename(source_file_name)

if __name__ == '__main__':
    TIME = TimeExecution()
    LOGGER = logging.getLogger('deploy-pipeline')
    DATE_FORMAT = """%(asctime)s,%(msecs)d %(levelname)-2s """
    INFO_FORMAT = """[%(filename)s:%(lineno)d] %(message)s"""
    LOG_FORMAT = DATE_FORMAT + INFO_FORMAT
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
    PARAMS = ReadParams(sys.argv)
    JAR_FILENAME = os.environ.get("JAR_FILENAME")
    BUCKET_NAME = os.environ.get("BUCKET_NAME")
    BUCKET_DESTINATION = os.environ.get("BUCKET_DESTINATION")
    upload_blob(BUCKET_NAME,
                JAR_FILENAME,
                BUCKET_DESTINATION)
    TIME.get_time()
    LOGGER.info('Process ended successfully.')
