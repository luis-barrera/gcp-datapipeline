package com.myCompany.base

import org.apache.spark.sql.SparkSession
import com.myCompany.utils.date
import com.myCompany.utils.readParams

object main {
  def main(args: Array[String]){
    //Spark variables
    val spark = SparkSession.builder()
      .appName("spark-bigquery-demo")
      .getOrCreate()
  
    val bucket = "spark-datalake-scala-jars"
    spark.conf.set("temporaryGcsBucket", bucket)

    // Load data in from BigQuery.
    val wordsDF = spark.read.format("bigquery")
      .option("table","gs://spark-datalake-raw-data/corpus/moby-dick.txt")
      .load()
      .cache()
    wordsDF.createOrReplaceTempView("words")

    // Perform word count.
    val wordCountDF = spark.sql(
      "SELECT word, SUM(word_count) AS word_count FROM words GROUP BY word")
    wordCountDF.show()
    wordCountDF.printSchema()

    // Saving the data to BigQuery.
    wordCountDF.write.format("bigquery")
      .option("table","corpus.wordcount_moby-dick")
      .save()
  }
}