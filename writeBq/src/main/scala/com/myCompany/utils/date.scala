package com.myCompany.utils

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

class date() {
    private var today: String = ""
    private val dateFmt = "yyyy-MM-dd HH:mm:ss.SSSS"
    private val date = new Date
    private val sdf = new SimpleDateFormat(dateFmt)
    this.today = sdf.format(date)

    def getToday(): String = {
        return this.today
    }    
}