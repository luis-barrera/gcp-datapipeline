#!/bin/bash

MODULE_COMPILE=""

function GET_BUILD_MODULE(){
    GIT_LAST_COMMIT=$(git log --oneline | head -1 | awk '{print $1}')
    BUILD_BRANCH=$(git branch | grep "*" | awk '{print $2}')
    echo "GIT_CURRENT_BRANCH: ${BUILD_BRANCH}"
    echo "GIT_LAST_COMMIT: ${GIT_LAST_COMMIT}"
    MODULE_COMPILE=$(ls -dl */ | grep -v "scripts" | awk '{print $9}')
}

function DEPLOY_MODULE(){
    if [ -z "${MODULE_COMPILE}" ] || [ "${MODULE_COMPILE}" == "scripts" ];
    then
        echo "No changes detected."
    else
        COUNT_MODULES=$(echo "${MODULE_COMPILE}" | wc -l)
        let INCREMENT=1
        while [ ${INCREMENT} -le ${COUNT_MODULES} ];
        do
            ## Install python dependencies
            MODULE=$(echo "${MODULE_COMPILE}" | head -${INCREMENT} | tail -1)
            echo "make -C ${MODULE} install-dependencies"
            make -C ${MODULE} install-dependencies
            ## Deploy to gcp
            JAR=$(ls -lha ${MODULE}target/*.jar | awk '{print $9}')
            PATH_JAR=$(pwd)
            export JAR_FILENAME=${PATH_JAR}/${JAR}
            export BUCKET_DESTINATION=${BUCKET_DESTINATION}${JAR}
            echo "make -C ${MODULE} deploy-gcp"
            make -C ${MODULE} deploy-gcp
            let INCREMENT=${INCREMENT}+1
        done
    fi
}

GET_BUILD_MODULE
DEPLOY_MODULE

