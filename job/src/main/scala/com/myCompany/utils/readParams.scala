package com.myCompany.utils

class readParams() {
    private var inputFile: String = ""
    private var outputFile: String = ""

    def getInputFile(): String = {
        return this.inputFile
    }

    def getOutputFile(): String = {
        return this.outputFile
    }

    def getParams(args: Array[String]): Unit = {
        if(args.length > 0){
            for(param <- args){
                if(param.contains("=")){
                    val splitParam = param.split("=")
                    validateParams(splitParam(0), splitParam(1))
                }
            }
        }
    }

    def validateParams(key: String, value: String): Unit = {
        if(key.equalsIgnoreCase("inputFile")){
            this.inputFile = value
        }
        if(key.equalsIgnoreCase("outputFile")){
            this.outputFile = value
        }
    }

    def getExtensionFile(filename: String): String = {
        val splitFilename = filename.split("/")
        val extensionFilename = splitFilename(splitFilename.length - 1).replace(".", "/")
        if(extensionFilename.contains("/")){
            val splitExtension = extensionFilename.split("/")
            return splitExtension(splitExtension.length - 1)
        }
        else{
            return "unknown"
        }
    }
}