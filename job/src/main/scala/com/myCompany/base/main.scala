package com.myCompany.base

import org.apache.spark.SparkContext._
import org.apache.spark.{SparkConf,SparkContext}
import com.myCompany.utils.date
import com.myCompany.utils.readParams

object main {
  def main(args: Array[String]){
    //Spark variables
    val conf = new SparkConf().setAppName("pipelineBase")
    val sc = new SparkContext(conf)
    //Custom objects
    val params = new readParams()
    params.getParams(args)
    val date = new date()
    println("Current date is " + date.getToday())
  }
}